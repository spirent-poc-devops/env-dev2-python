# -*- coding: utf-8 -*-
"""
    common.values
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to handle nested dictionaries
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

def get_map_value(map, key, default = None):
    names = key.split('.')
    obj = map

    for name in names:
        if obj == None:
            break
        elif isinstance(obj, list):
            index = int(name)
            obj = obj[index]
        elif isinstance(obj, dict):
            obj = obj[name] if name in obj else None
        else:
            raise Exception("Cannot get value by key " + name + ". The parent property must be a list or dict")

    if obj == None:
        obj = default

    return obj


def set_map_value(map, key, value):
    names = key.split('.')

    if len(names) <= 1:
        if isinstance(map, list):
            index = int(key)
            map[key] = value
        else:
            map[key] = value
        return

    name = names[0]
    if isinstance(map, list):
        index = int(name)
        nested_map = map[index] if index in map else None
        if nested_map == None:
            nested_map = dict()
            map[index] = nested_map
    else:
        nested_map = map[name] if name in map else None
        if nested_map == None:
            nested_map = dict()
            map[name] = nested_map

    nested_key = ".".join(names[1:])
    set_map_value(nested_map, nested_key, value)    


def get_map_keys(map):
    keys = []
    if isinstance(map, list):
        for index in range(len(map)):
            nested_map = map[index]
            nested_keys = get_map_keys(nested_map)
            if len(nested_keys) == 0:
                keys.append(str(index))
            else:
                for nested_key in nested_keys:
                    key = str(index) + "." + nested_key
                    keys.append(key)
    elif isinstance(map, dict):
        for key in map:
            nested_map = map[key]
            nested_keys = get_map_keys(nested_map)
            if len(nested_keys) == 0:
                keys.append(key)
            else:
                for nested_key in nested_keys:
                    compound_key = key + "." + nested_key
                    keys.append(compound_key)

    return keys


def set_map_default(map, default):
    if not isinstance(default, dict):
        return
    if not isinstance(map, dict):
        return

    for key in default:
        map_value = map[key] if key in map else None
        default_value = default[key]

        if map_value == None:
            map[key] = default_value
        elif isinstance(map_value, list) and len(map_value) == 0:
            map[key] = default_value
        elif isinstance(map_value, dict):
            set_map_default(map_value, default_value)


def test_map_value(map, key):
    if map == None:
        return False

    names = key.split('.')

    if len(names) <= 1:
        if isinstance(map, list):
            index = int(key)
            return len(map) > index
        elif isinstance(map, dict):
            return key in map
        else:
            return False

    name = names[0]
    if isinstance(map, list):
        index = int(name)
        nested_map = map[index] if index in map else None
    else:
        nested_map = map[name] if name in map else None

    nested_key = ".".join(names[1:])
    return test_map_value(nested_map, nested_key)


def remove_map_value(map, key):
    if map == None:
        return

    names = key.split('.')

    if len(names) <= 1:
        if isinstance(map, list):
            index = int(key)
            if index in map:
                map.pop(key)
        if isinstance(map, dict):
            if key in map:
                del map[key]
        return

    name = names[0]
    if isinstance(map, list):
        index = int(name)
        nested_map = map[index] if index in map else None
    else:
        nested_map = map[name] if name in map else None

    nested_key = ".".join(names[1:])
    remove_map_value(nested_map, nested_key)
