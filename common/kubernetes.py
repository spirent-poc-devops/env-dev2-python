# -*- coding: utf-8 -*-
"""
    common.kubernetes
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to control kubernetes
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from .execute import run_process, read_process

def switch_kube_context():
    current_context = read_process("kubectl config current-context")

    if current_context != "minikube":
        exit_code = run_process("kubectl config use-context minikube")

        if exit_code != 0:
           raise Exception("There were errors switching to minikube context, Watch logs above");
