# -*- coding: utf-8 -*-
"""
    common.logging
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to product log output
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import sys
from datetime import datetime

env_log_file = None

def write_log(level, component, message, delimiter = False, color = "White"):
    global env_log_file

    # Determine the log file name
    if env_log_file == None:
        now = datetime.now()
        log_time = now.strftime("%Y-%m-%d_%H-%M-%S")
        file_name = "env" + "_" + log_time + ".log"
        env_log_file = file_name
    else:
        file_name = env_log_file

    # Calculate the file path
    if file_name == None or file_name == "":
        path = os.getcwd() + "/logs/dev_env.log"
    else:
        path = os.getcwd() + "/logs/" + file_name

    # Create the file if it doesn't exist
    if not os.path.exists(path):
        with open(path, 'w') as f:
            f.write("")

    # If resources do not exist yet return an empty result
    if level == None or message == "":
        raise Exception("Missing the log level or message")
    
    # Append message to the log
    now = datetime.now()
    timestamp = now.strftime("%Y/%m/%d %H:%M:%S")
    line = '{"component":"' + component + '","time":"' + timestamp + '","level":"' + level + '","message":"' + message + '"}\n'
    with open(path, 'a', encoding='utf-8') as f:
        f.write(line)

    # Define a delimiter line
    if delimiter:
        message = "\n***** " + message + " *****\n"

    # Write the message to console
    if level == "Error":
        print(message, file=sys.stderr)
    else:
        print(message)


def write_error(component, message, delimiter = False, color = "Red"):
    write_log("Error", component, message, delimiter, color)


def write_info(component, message, delimiter = False, color = "White"):
    write_log("Info", component, message, delimiter, color)


def write_warn(component, message, delimiter = False, color = "White"):
    write_log("Warn", component, message, delimiter, color)


def write_debug(component, message, delimiter = False, color = "White"):
    write_log("Debug", component, message, delimiter, color)
