# -*- coding: utf-8 -*-
"""
    commons.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Contains the implementation of common functions used in the environment provisioning scripts.
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

__all__ = [
    'get_aws_output',
    'format_duration', 'format_table',
    'run_process', 'run_process_safe', 'read_process', 'is_linux', 'is_windows', 'is_macos',
    'get_map_value', 'set_map_value', 'get_map_keys', 'set_map_default', 'test_map_value', 'remove_map_value',
    'convert_from_template', 'build_template',
    'read_config', 'convert_to_resource_path', 'read_resources', 'write_resources',
    'write_log', 'write_error', 'write_warn', 'write_info', 'write_debug',
    'parse_cmdline',
    'switch_kube_context'
]

from .convert import get_aws_output
from .format import format_duration, format_table
from .execute import run_process, run_process_safe, read_process, is_linux, is_windows, is_macos
from .values import get_map_value, set_map_value, get_map_keys, set_map_default, test_map_value, remove_map_value
from .templates import convert_from_template, build_template
from .config import read_config, convert_to_resource_path, read_resources, write_resources
from .logging import write_log, write_error, write_warn, write_info, write_debug
from .cmdline import parse_cmdline
from .kubernetes import switch_kube_context
