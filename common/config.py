# -*- coding: utf-8 -*-
"""
    common.config
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to process config and resource files
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import json
from .values import set_map_default

def read_config(config_path):
    if config_path == None or config_path == "":
        raise Exception("Config path is not set. Execute <script>.ps1 -Config <config file>")

    if not os.path.exists(config_path):
        raise Exception("Config path $($ConfigPath) was not found")

    # Read the config file
    with open(config_path, 'r', encoding='utf-8') as f:
        config = json.load(f)

    # Apply default 
    path = os.path.abspath(os.path.dirname(__file__))
    default_config_path =  path + "/../config/default_config.json"
    if os.path.exists(default_config_path):
        with open(default_config_path, 'r', encoding='utf-8') as f:
            default_config = json.load(f)
        set_map_default(config, default_config)

    return config


def convert_to_resource_path(config_path):
    parent = os.path.dirname(config_path)
    file = os.path.basename(config_path)
    last_dot_pos = file.rindex('.')

    if last_dot_pos > -1:
        file = file[0:last_dot_pos]

    if "config" in file:
        file = file.replace("config", "resources")
    else:
        file = file + "_resources"

    return parent + "/" + file + ".json"


def read_resources(resource_path):
    # If resources do not exist yet return an empty result
    if resource_path == None or resource_path == "":
        return dict()

    if not os.path.exists(resource_path):
        return dict()

    # Read the resources file
    with open(resource_path, 'r', encoding='utf-8') as f:
        resources = json.load(f)
    return resources


def write_resources(resource_path, resources):
    # Write the content to resource file
    with open(resource_path, 'w', encoding='utf-8') as f:
        json.dump(resources, f, indent=2)
