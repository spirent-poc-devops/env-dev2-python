# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks 2.0 Development Environment V2

This is the environment automation scripts to create, update and delete Development Environments. The Development environments are functionally 
identical to production environments, but able to fit and run on a regular development computer. 

The environment consists of the following components:
* 1-node **Kubernetes** cluster running as minikube (docker desktop)
* **Environment** config map with information about the environment

In the 2nd major update of the environment all infrastructure were moved out of the environment and placed into the [package-infraservices-helm](https://git.vwx.spirent.com/vwx-delivery/package-infraservices-helm) component

<img src="design.png" width="800">

<a name="links"></a> Quick links:

* [Delivery Platform Requirements](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Requirements)
* [Delivery Platform Architecture](https://frkengjira01.spirentcom.com/confluence/display/VWX/Delivery+Platform+Architecture)
* [Change Log](CHANGELOG.md)
* [Infrastructure Services](https://git.vwx.spirent.com/vwx-delivery/package-infraservices-helm)


# Project structure
| Folder | Description |
|----|----|
| common | Scripts with support functions like working with configs, templates etc. | 
| config | Config files for scripts. Also stores *resources* files, created automaticaly. | 
| kubernetes | Contains scripts for provisioning the kubernetes component. |
| environment | Config map with information about the environment. |


## Use

Start the process from any computer. Download released package with environment provisioning scripts from http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-dev-python-2.0.0.zip
```bash
wget http://artifactory.vwx.spirent.com/misc-binaries/vwx-delivery/env-dev-python-2.0.0.zip
```

Unzip scripts from the package
```bash
unzip env-dev-python-2.0.0.zip
```

Go to the `config` folder and create configuration for a new environment.
Read configuration section below and use [sample_config.json](config/sample_config.json)
as your starting point.

Your configuration may look like the sample below.
```json
{
  "environment": {
    "type": "dev",
    "version": "1.0.0"
  },
  "k8s": {
    "minikube_home": "",
    "memory": 4096,
    "version": "v1.22.0",
    "cpus": 2,
    "namespace": "infra"
  }
}
```

Install prerequisites on your computer by running the installation script for your platform.
```bash
install_prereq_[win | mac | ubuntu].[ps1 | sh]
```

Create the environment:
```bash
python create_env.py --config=<path to config file>
```

After you run the script you shall see a resource file (ending with `resources.json`) next to the config file you created.

Delete the environment when its no longer needed to free up used computing resources.
```bash
python delete_env.py --config=<path to config file>
```

## Accessing apps

A loadbalancer is installed within minikube to access services inside the cluster based on ingress rules deployed. To expose the loadbalancer externally you need to run the following command:

```bash
minikube tunnel
```

Loadbalancer would be exposed at `127.0.0.1`. More details [here](https://minikube.sigs.k8s.io/docs/handbook/accessing/#loadbalancer-access)

## Configuration

The environment configuration supports the following configuration parameters:

| Parameter | Default value | Description |
|----|----|---|
| type | dev | Type of environment |
| k8s.version | v1.9.4 | Kuberntes cluster version |
| k8s.memory | 8198 | Memory allocated to minikube vm |
| k8s.cpus | 2 | Minikube cpu count |
| k8s.namespace | infra | Namespace where to put infrastructure components |


## Resources

As the result of the scripts information about created resources will be saved into a resource file that ends with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters list:
| Parameter | Ex. value | Description |
|----|----|---|
|k8s.type| minikube | Type of k8s deployment |
|k8s.address| 192.168.99.100 | IP address of the k8s node |
|k8s.ssh_key| C:\\Users\\DevUser\\.minikube\\machines\\minikube\\id_rsa | Location of k8s' ssh key |

Example environment resources file:
```json
{
  "environment": {
    "create_time": "2021-09-01T12:37:51.4650127-07:00",
    "delete_time": "2021-09-01T07:27:42.0670345-07:00",
    "version": "1.0.0",
    "type": "dev"
  },
  "k8s": {
    "version": "v1.22.0",
    "type": "minikube",
    "cpus": 2,
    "address": "192.168.49.2",
    "ssh_key": "C:\\Users\\XXX\\.minikube\\machines\\minikube\\id_rsa",
    "namespace": "infra",
    "memory": 4096
  }
}
```

## Known issues

* Setting low cpu and memory values in the configuration file can cause problems with pods starting up. For example, 2 cpu and 2 GB memory caused problems with the Kakfa pod during testing. Best results are achieved with 4 cpu and 6+ GB memory. 
* Install prerequisite script for mac needs to have different brew commands based on the brew version. The script is written assuming brew version 3 and above: [troubleshooting](https://stackoverflow.com/questions/30413621/homebrew-cask-option-not-recognized)
* Scripts require helm v3 which is installed using the prerequisite script. If you have helm v2 already installed then you need to switch to helm v3 for the scripts to work: [details](https://medium.com/faun/easily-switch-between-helm2-and-helm3-using-bash-function-64d2740b5091)
* On Windows 10, when using Docker driver, ports <1024 don't work with the default latest version of OpenSSH (OpenSSH_for_Windows_7.7p1, LibreSSL 2.6.5): [details](https://minikube.sigs.k8s.io/docs/handbook/accessing/#access-to-ports-1024-on-windows-requires-root-permission).
To fix it, OpenSSH must be uninstalled from Settings > Apps > Optional Features, and then installed using chocolatey (this command is included in the install_prereq_win.ps1 script). Opening a new powershell window and running `ssh -V` should return the new 8.0 version. After this, minikube tunnel will work successfully.

## Contacts

This environment was created and currently maintained by the team managed by *Sergey Seroukhov* and *Raminder Singh*.

