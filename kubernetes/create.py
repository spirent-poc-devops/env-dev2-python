# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import time
from common import read_config, read_resources, write_resources
from common import test_map_value, get_map_value, set_map_value
from common import run_process, run_process_safe, read_process, is_linux, is_windows, is_macos
from common import write_error, write_info, write_warn
from common import switch_kube_context

def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    # If k8s cluster was created then exit
    if test_map_value(resources, "k8s") and test_map_value(resources, "k8s.type"):
        write_info("kubernetes", "Kubernetes has been created. Skipping...")
        return

    cpus = get_map_value(config, "k8s.cpus")
    memory = get_map_value(config, "k8s.memory")
    version = get_map_value(config, "k8s.version")
    minikube_home = get_map_value(config, "k8s.minikube_home")

    # If using the docker driver, check that docker is running
    print("Checking docker status:")
    output = read_process("docker info")
    if "error during connect" in output:
        print('Received "error during connect" - attempting to start docker...')

        # try to start docker
        if is_macos():
            if not os.path.exists("/Applications/Docker.app"):
                write_warn("kubernetes", "Warning: docker not found at /Applications/Docker.app")
            run_process_safe("/Applications/Docker.app")

        elif is_linux():
            run_process_safe("systemctl start docker")

        elif is_windows():
            if not os.path.exists('C:\Program Files\Docker\Docker\Docker Desktop.exe'):
                write_warn("kubernetes", "Warning: docker not found at C:\Program Files\Docker\Docker\Docker Desktop.exe")
            run_process_safe("& 'C:\Program Files\Docker\Docker\Docker Desktop.exe'")

        # Give docker a few minutes to start
        print("Waiting 2 minutes for docker to start...")

        # Todo: Refactor to wait until docker is available
        time.sleep(120)
        print("Checking docker status")
        output = read_process("docker info")
        if "error during connect" in output:
            write_error("kubernetes", "Could not start docker")
            raise Exception("Could not start docker")

    print("Docker seems to be running. Continue...")

    # Set minikube home directory
    if minikube_home != None and minikube_home != "":
        os.environ["MINIKUBE_HOME"] = minikube_home

    # Compare installed version to version set in config
    local_minikube_version = read_process("minikube version --short")
    set_minikube_version = get_map_value(config, "k8s.version")
    if local_minikube_version > set_minikube_version:
        write_warn("kubernetes", "Locally installed minikube version " + local_minikube_version \
            + " does not match the version in the environment config " + set_minikube_version + ". " \
            + "Please update config's k8s.version to match the locally installed version.")

    # Start minikube
    run_process_safe("minikube start --cpus=" + str(cpus) \
        + " --memory=" + str(memory) \
        + " --driver=docker " \
        + " --kubernetes-version=" + version)

    k8s_address = read_process("minikube ip")
    k8s_ssh_key = read_process("minikube ssh-key")

    # Record results and save them to disk
    set_map_value(resources, "k8s.type", "minikube")
    set_map_value(resources, "k8s.address", k8s_address)
    set_map_value(resources, "k8s.public_ip", k8s_address)
    set_map_value(resources, "k8s.ssh_key", k8s_ssh_key)
    # Record current k8s configuration to resources
    set_map_value(resources, "k8s.version", version)
    set_map_value(resources, "k8s.memory", memory)
    set_map_value(resources, "k8s.cpus", cpus)

    write_resources(resources_path, resources)

    # Wait for minikube to start
    # Todo: rewrite to use "minikube status"
    while True:
        print("Waiting for minikube to start...")
        time.sleep(5)
        output = read_process("kubectl get nodes")
        if "Ready" in output:
            break

    # Enable ingress controller
    # minikube addons enable ingress

    switch_kube_context()
