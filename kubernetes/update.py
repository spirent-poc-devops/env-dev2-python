# -*- coding: utf-8 -*-
"""
    component.update
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to update a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from common import read_config, read_resources
from common import get_map_value
from common import write_error, write_info

def update(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    if get_map_value(resources, "k8s.version") != get_map_value(config, "k8s.version") \
        or get_map_value(resources, "k8s.driver") != get_map_value(config, "k8s.driver") \
        or get_map_value(resources, "k8s.memory") != get_map_value(config, "k8s.memory") \
        or get_map_value(resources, "k8s.cpus") != get_map_value(config, "k8s.cpus") :
        write_error("kubernetes", "Kubernetes configuration changed, to update the environment you need to entirely delete it (delete_env.ps1) and recreate it (create_env.ps1).")
        raise Exception("Minikube update is not supported")
    else:
        write_info("kubernetes", "Kibernetes configuration didn't change. Skipping...")
