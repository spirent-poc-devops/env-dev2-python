# -*- coding: utf-8 -*-
"""
    component.delete
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from common import read_config, read_resources, write_resources
from common import remove_map_value
from common import run_process
from common import write_error

def delete(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    # Destroy minikube
    exit_code = run_process("minikube delete")

    if exit_code != 0:
        write_error("kubernetes", "There were errors deleting minikube, Watch logs above")
        raise Exception("Cannot delete minikube")

    # Delete results and save resource file to disk
    remove_map_value(resources, "k8s.type")
    remove_map_value(resources, "k8s.address")
    remove_map_value(resources, "k8s.public_ip")
    remove_map_value(resources, "k8s.ssh_key")
    remove_map_value(resources, "k8s.memory")
    remove_map_value(resources, "k8s.version")
    remove_map_value(resources, "k8s.cpus")
    write_resources(resources_path, resources)
